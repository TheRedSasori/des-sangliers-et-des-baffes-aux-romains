var hierarchy =
[
    [ "MonoBehaviour", null, [
      [ "ChangeScene", "class_change_scene.html", null ],
      [ "CharacterController2D", "class_character_controller2_d.html", null ],
      [ "EnemyController", "class_enemy_controller.html", null ],
      [ "EnemySpawn", "class_enemy_spawn.html", null ],
      [ "GameOver", "class_game_over.html", null ],
      [ "Health", "class_health.html", null ],
      [ "HealthBar", "class_health_bar.html", null ],
      [ "PauseMenu", "class_pause_menu.html", null ],
      [ "PlayerController", "class_player_controller.html", null ],
      [ "sanglier", "classsanglier.html", null ]
    ] ]
];