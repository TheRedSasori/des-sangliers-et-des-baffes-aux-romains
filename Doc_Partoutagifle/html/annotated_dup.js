var annotated_dup =
[
    [ "ChangeScene", "class_change_scene.html", "class_change_scene" ],
    [ "CharacterController2D", "class_character_controller2_d.html", "class_character_controller2_d" ],
    [ "EnemyController", "class_enemy_controller.html", "class_enemy_controller" ],
    [ "EnemySpawn", "class_enemy_spawn.html", "class_enemy_spawn" ],
    [ "GameOver", "class_game_over.html", "class_game_over" ],
    [ "Health", "class_health.html", "class_health" ],
    [ "HealthBar", "class_health_bar.html", null ],
    [ "PauseMenu", "class_pause_menu.html", "class_pause_menu" ],
    [ "PlayerController", "class_player_controller.html", "class_player_controller" ],
    [ "sanglier", "classsanglier.html", "classsanglier" ]
];