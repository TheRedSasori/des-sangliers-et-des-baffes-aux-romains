﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour
{
    [SerializeField]
    private int maxHealth = 100;

    [SerializeField]
    public string name = "default";

    private int currentHealth;

    public event Action<float> OnHealthPctChanged = delegate { };

    public void OnEnable()
    {
        currentHealth = maxHealth;
    }

    public int getHealth()
    {
        return this.currentHealth;
    }

    public void ModifyHealth(int amount)
    {
        currentHealth += amount;

        float currentHealthPct = (float)currentHealth / (float)maxHealth;
        OnHealthPctChanged(currentHealthPct);
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

    }
}
