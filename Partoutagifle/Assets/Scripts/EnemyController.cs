﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    private GameObject wayPoint;
    private PlayerController player;
    private Vector3 wayPointPos;
    public float speed = 3.0f;
    public float attackDamages = 10;

    Animator anim;
    Rigidbody2D rgb2D;
    private Vector3 scale;
    private float initialscaleX;
    private Renderer renderer;
    public float scalevalue = 4;

    void Start()
    {
        wayPoint = GameObject.Find("wayPoint");
        anim = GetComponent<Animator>();
        rgb2D = GetComponent<Rigidbody2D>();
        scale = transform.localScale;
        renderer = GetComponent<SpriteRenderer>();
        this.player = GameObject.Find("player").GetComponent<PlayerController>();
    }

    void Update()
    {
        if(GetComponent<Health>().getHealth() <= 0)
        {
            Destroy(this.gameObject);
        }

        if(playerOnLeft()) {
            scale.x = scalevalue;
        } else {
            scale.x = -scalevalue;
        }
        transform.localScale = scale;

        wayPointPos = new Vector3(wayPoint.transform.position.x, wayPoint.transform.position.y, 0f);
        transform.position = Vector3.MoveTowards(transform.position, wayPointPos, speed * Time.deltaTime);
    }

    bool playerOnLeft(){
        if(wayPoint.transform.position.x > transform.position.x) {
            return true;
        } else {
            return false;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            anim.SetBool("isOnRange", true);
            // collision.gameObject.SendMessage("ApplyDamage", 10);
            this.player.TakeDamages(this.attackDamages * -1);
        } else
        {
            anim.SetBool("isOnRange", false);
        }

        if(collision.gameObject.tag == "Sanglier")
        {
            GetComponent<Health>().ModifyHealth(-10);
        }
    }
}
