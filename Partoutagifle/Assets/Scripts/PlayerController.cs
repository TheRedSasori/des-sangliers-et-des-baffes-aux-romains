﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public CharacterController2D controller;

    public bool téléscopage;
    public float runSpeed = 40f;
    public int PlayerDamages = 1;
    float horizontalMove = 0f;
    bool jump = false;
    bool crouch = false;
    int cpt = 0;
    public float distance = 2f;

    [SerializeField]
    private Animator m_Animator;

    [SerializeField]
    private GameObject sanglier;

    [SerializeField]
    int force = 5;

    RaycastHit2D hit;
    private GameObject sanglierObj;
    //In the editor, add your wayPoint gameobject to the script.
    public GameObject wayPoint;
    public GameObject bulletSpawn;
    //This is how often your waypoint's position will update to the player's position
    private float timer = 0.5f;
    public float throwForce;
    public bool isLookingRight;
    private Renderer renderer;
    public bool visible;

    void Start(){
        isLookingRight = true;
        renderer = GetComponent<Renderer>();
    }
    // Update is called once per frame
    void Update()
    {
        float life = GetComponent<Health>().getHealth();

        if (life <= 0)
        {
            GameOver.gameIsOver = true;
        }

        if(téléscopage){
            tryTelescop();
        } else {
            Destroy(this.gameObject);
        }

        if(!renderer.isVisible) {
            visible = false;
        } else {
            visible = true;
        }

        horizontalMove = Input.GetAxisRaw("Horizontal") * runSpeed;
        if (Input.GetAxisRaw("Horizontal") != 0)
        {
            m_Animator.SetBool("isWalking", true);
        }
        else
        {
            m_Animator.SetBool("isWalking", false);
        }


        if (Input.GetButtonDown("Jump") || Input.GetKey(KeyCode.UpArrow))
        {
            jump = true;
        }

        if (Input.GetButtonDown("Crouch") || Input.GetKeyDown(KeyCode.DownArrow))
        {
            crouch = true;
        }
        else if (Input.GetButtonUp("Crouch") || Input.GetKeyUp(KeyCode.DownArrow))
        {
            crouch = false;
        }

        if (Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.D))
        {
            isLookingRight = true;
        }
        if (Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.Q))
        {
            isLookingRight = false;
        }

        if (timer > 0)
        {
            timer -= Time.deltaTime;
        }
        if (timer <= 0)
        {
            //The position of the waypoint will update to the player's position
            UpdatePosition();
            timer = 0.5f;
        }

        if (Input.GetKey(KeyCode.F))
        {
            cpt++;
            if (cpt == 1)
            {
                StartCoroutine(InstantiateSanglier());
            }

        }

    }

    void tryTelescop(){
        float posX = transform.position.x;
        if(posX < -16f || posX > 16f){
            var pos = transform.position;
            transform.position = new Vector3(-pos.x, pos.y, pos.z);
        }
        float posY = transform.position.y;
        if(posY < -8f || posY > 8f){
            var pos = transform.position;
            transform.position = new Vector3(pos.x, -pos.y, pos.z);
        }

    }


    public void TakeDamages(float damages)
    {
        GetComponent<Health>().ModifyHealth((int)damages);
    }

    void UpdatePosition()
    {
        //The wayPoint's position will now be the player's current position.
        wayPoint.transform.position = transform.position;
    }

    void FixedUpdate()
    {
        // Move our character
        controller.Move(horizontalMove * Time.fixedDeltaTime, crouch, jump);
        jump = false;
    }

    IEnumerator InstantiateSanglier()
    {
        sanglierObj = (GameObject)Instantiate(sanglier, bulletSpawn.transform.position, Quaternion.identity);
        sanglierObj.gameObject.GetComponent<Rigidbody2D>().AddForce(sanglierObj.transform.up * 300);
        if (isLookingRight)
        {
            sanglierObj.gameObject.GetComponent<Rigidbody2D>().AddForce(sanglierObj.transform.right * 1000);
        } else
        {
            sanglierObj.gameObject.GetComponent<Rigidbody2D>().AddForce(sanglierObj.transform.right * -1000);
        }
        

        yield return new WaitForSeconds(0.5f);
        cpt = 0;
    }
}
