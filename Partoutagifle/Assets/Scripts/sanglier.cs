﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class sanglier : MonoBehaviour
{
    private GameObject player;
    private SpriteRenderer renderer;
    private Rigidbody2D rgb2D;
    private bool rendererstate;
    public bool téléscopage;

    public Transform particles;

    void Start(){
        player = GameObject.Find("wayPoint");
        renderer = GetComponent<SpriteRenderer>();
        rendererstate = true;
        rgb2D = GetComponent<Rigidbody2D>();
        StartCoroutine(blink());
    }

    // Update is called once per frame
    void Update()
    {
        float posX = transform.position.x;
        if(téléscopage){
            if(posX < -16f || posX > 16f){
                telescop();
            }
        } else {
            if(posX < -16f || posX > 16f){
                Destroy(this.gameObject);
            }
            
        }
        
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        particles.GetComponent<ParticleSystem>().Play();

        if (collision.gameObject.tag == "Ground")
        {
            Destroy(this.gameObject);
        }

        if(collision.gameObject.tag == "Enemy")
        {
            Destroy(this.gameObject);
        }
    }

    private void telescop(){
        var pos = transform.position;
        transform.position = new Vector3(-pos.x, pos.y, pos.z);
    }

    IEnumerator blink()
    {
        yield return new WaitForSeconds(2.5f);
        InvokeRepeating("onoff", 0, 0.1f);
    }

    private void onoff() {
            rendererstate = !rendererstate;
            renderer.enabled = rendererstate;
    }
}
