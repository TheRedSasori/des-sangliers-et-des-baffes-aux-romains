﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawn : MonoBehaviour
{
    public GameObject enemy;
    public GameObject boss;
    public float spawnTime = 5f;
    public int enemyCount = 0;
    // Use this for initialization
    void Start()
    {
        InvokeRepeating("SpawnBall", spawnTime, spawnTime);
    }

    // Update is called once per frame
    void Update()
    {
        if(enemyCount % 10 == 0 && enemyCount != 0){
            GameObject.Instantiate(boss);
            enemyCount++;
            if(spawnTime > 2f) {
                spawnTime = spawnTime - 0.5f;
                CancelInvoke();
                InvokeRepeating("SpawnBall", spawnTime, spawnTime);
            }
            
        }
    }
    void SpawnBall()
    {
        var newBall = GameObject.Instantiate(enemy);
        enemyCount++;
    }
}
